function eventos(){
	comprobarlogeo();
	cerrarSesion();	


}

function cerrarSesion(){
	
	$('#cerrarSesion').on('click',  function(){
		console.log("click en cerrar sesion");
		$.ajax
			({
			type: "POST",
		   	url: "modelo/consultasUsuario.php",
		   	data: {id:3},
			async: false,	
				
			success:
		    function () 
			{	
				
				window.open('index.php' , '_self');
		     },
		        error:
		        function () {alert( msg +"No se pudo realizar la conexion");}
			});
	});

}
function reloj(tipo) {
//Variables
     horareal = new Date();
    // console.log("Hora Real:"+horareal);
     hora = horareal.getHours();
     minuto = horareal.getMinutes();
     segundo = horareal.getSeconds();
//Codigo para evitar que solo se vea un numero en los segundos
     comprobarsegundo = new String (segundo);
     if (comprobarsegundo.length == 1)
         segundo = "0" + segundo;
//Codigo para evitar que solo se vea un numero en los minutos
     comprobarminuto = new String (minuto);
     if (comprobarminuto.length == 1)
         minuto = "0" + minuto;
//Codigo para evitar que solo se vea un numero en las horas
     comprobarhora = new String (hora);
     if (comprobarhora.length == 1)
         hora = "0" + hora;
// Codigo para mostrar el reloj en pantalla
     verhora = hora + " : " + minuto + " : " + segundo;
     if(tipo==1){
 	 	$('#reloj').val(verhora);
 	 	setTimeout("reloj("+tipo+")",1000);
 	 }
 	if(tipo==-1){
 	     reloj2.innerHTML=verhora;
 	     setTimeout("reloj("+tipo+")",1000);
 	 }
 	 if(tipo==2){
 	    return verhora;
 	     //setTimeout("reloj("+tipo+")",1000);
 	 }

    // console.log("hora: " + verhora);
     
}
function comprobarlogeo(){
	$.ajax
		({
		type: "POST",
	   	url: "modelo/consultasUsuario.php",
	   	data: {id:2},
		async: false,	
		dataType: "json",		
		success:
	    function (msg) 
		{	console.log(msg);
			
			if(msg[0]=="false"){	
				ActivarLogin();
			}
			if(msg[0]=="true"&&msg[1]=="Administrador"){
				$("#menuizquierdo").hide().load('partials/menuAdministrador.html', function(){
				console.log("Usuario Administrador");
				$('#cerrarSesion').fadeIn();
					cerrarSesion();
					eventosMenuAdministrador();
				}).fadeIn(1500);
			}
			if(msg[0]=="true"&&msg[1]=="Tecnico"){
				
				$("#menuizquierdo").hide().load('partials/menuTecnico.html', function(){
					eventosMenuTecnico();
					console.log("Usuario Tecnico");
					$('#cerrarSesion').fadeIn();
					cerrarSesion();
				}).fadeIn(1500);
			}
			if(msg[0]=="true"&&msg[1]=="Usuario General"){
				$("#menuizquierdo").hide().load('partials/menuUsuario.html', function(){
				console.log("Usuario General");
				$('#cerrarSesion').fadeIn();
					cerrarSesion();
					eventoMenuUsuario();
				}).fadeIn(1500);
			}
			

			
	     },
	        error:
	        function (msg) {alert( msg +"No se pudo realizar la conexion por favor comuniqueses con el administrador");}
		});
}


function eventoMenuprincipal(){
	

	$('#menuprincipal').on('click',  function(){

		console.log("click en menu principal");
		$("#contenedor").hide().load('partials/menuprincipal.html', function(){
			$('#cerrarSesion').fadeIn();
			comprobarlogeo();
			cerrarSesion();	
			//eventoMenuUsuario();

		}).fadeIn(1500);
	});
	
}
function eventosMenuTecnico(){
	$('#generarinformetecnico').on('click',  function(){
		
		$("#contenedor").hide().load('partials/informetecnico.php', function(){
			eventoMenuprincipal();
			cerrarSesion();
			reloj(-1);

			
		}).fadeIn(1500);

	});
	$('#consultadesolicitud').on('click',  function(){
		
		$("#contenedor").hide().load('partials/consultadesolicitudTecnico.html', function(){
			eventoMenuprincipal();
			cerrarSesion();
			
		}).fadeIn(1500);

	});
}
function eventosMenuAdministrador(){
	$('#administrarusuarios').on('click',  function(){
		
		$("#contenedor").hide().load('partials/datosfuncionario.html', function(){
			$('#cerrarSesion').fadeOut();
			eventoMenuprincipal();
			cerrarSesion();
		}).fadeIn(1500);

	});
	$('#administrarequipos').on('click',  function(){
		
		$("#contenedor").hide().load('partials/registrarEquipo.html', function(){
			$('#cerrarSesion').fadeOut();
			eventoMenuprincipal();
			cerrarSesion();
		}).fadeIn(1500);

	});
	$('#solicituddeservicio').on('click',  function(){
		$("#contenedor").hide().load('partials/SolicituddeservicioAdministrador.php', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
	$('#asignartecnico').on('click',  function(){
		$("#contenedor").hide().load('partials/asignartecnico.html', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
	$('#verInforme').on('click',  function(){
		$("#contenedor").hide().load('partials/verinformeAdministrador.html', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
	$('#solicitudesasignadas').on('click',  function(){
		$("#contenedor").hide().load('partials/solicitudesasignadas.html', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
}
function eventoMenuUsuario(){
	$('#solicituddeservicio').on('click',  function(){
		$("#contenedor").hide().load('partials/solicituddeservicio.php', function(){
			eventoMenuprincipal();
			cerrarSesion();
			
			reloj(1);
		/*   horareal = new Date();
		   var fecha=horareal.getDate()+"-"+horareal.getMonth()+"-"+horareal.getFullYear();
		   console.log("Fecha actual: "+fecha);
		   $('#Fecha').val(fecha);
		   Fecha.value=fecha;
*/			 

		}).fadeIn(1500);

	});
	$('#consultar').on('click',  function(){
		$("#contenedor").hide().load('partials/UsuarioConsultadeSolicitud.html', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
	$('#tiempo').on('click',  function(){
		$("#contenedor").hide().load('partials/tiemposderespuesta.html', function(){
			$('#cerrarSesion').fadeOut();
			comprobarlogeo();
			cerrarSesion();	
			eventoMenuprincipal();
			
		}).fadeIn(1500);

	});
}


function ActivarLogin(){
	console.log("entro en panel activar login");
			$("#menuizquierdo").hide().load('partials/login.html', function(){
				$('#iniciarsesion').on('click',  function(){
						if(temail.value!=""&&tpass.value!=""){
							$.ajax
							({
							type: "POST",
						   	url: "modelo/consultasUsuario.php",
						   	data: {id:1, temail:temail.value, tpass:tpass.value},
							async: false,			
							success:
						    function (msg) 
							{	console.log(msg);
								
								if(msg=="false"){		
									alertas.innerHTML="<div class='alert alert-danger'>Datos Invalidos</div>";
								}
								else{comprobarlogeo();}
								

								
						     },
						        error:
						        function (msg) {alert( msg +"No se pudo realizar la conexion Comuniquese con el administrador por favor");}
							});
							
							}
							else alertas.innerHTML="<div class='alert alert-danger'>Llene con sus datos</div>";
				
			});


			}).fadeIn(1500);
	}