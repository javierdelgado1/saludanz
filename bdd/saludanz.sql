-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-12-2014 a las 02:21:44
-- Versión del servidor: 5.6.15-log
-- Versión de PHP: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `saludanz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

CREATE TABLE IF NOT EXISTS `asignaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `idsolicitud` int(11) NOT NULL,
  `diagnostico` varchar(200) DEFAULT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `solucion` varchar(200) DEFAULT NULL,
  `soporte` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Volcado de datos para la tabla `asignaciones`
--

INSERT INTO `asignaciones` (`id`, `idusuario`, `idsolicitud`, `diagnostico`, `observacion`, `solucion`, `soporte`) VALUES
(6, 5, 12, '', NULL, NULL, NULL),
(26, 3, 11, '', NULL, NULL, NULL),
(29, 3, 13, '', NULL, NULL, NULL),
(35, 3, 14, 'sf', 'd', 'fd', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informe`
--

CREATE TABLE IF NOT EXISTS `informe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idTecnico` int(11) NOT NULL,
  `idsolicitud` int(11) NOT NULL,
  `diagnostico` varchar(200) NOT NULL,
  `solucion` varchar(200) NOT NULL,
  `observacion` varchar(200) NOT NULL,
  `soporte` varchar(50) NOT NULL,
  `fechainforme` date NOT NULL,
  `hora` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `informe`
--

INSERT INTO `informe` (`id`, `idTecnico`, `idsolicitud`, `diagnostico`, `solucion`, `observacion`, `soporte`, `fechainforme`, `hora`) VALUES
(4, 3, 14, 'a', 'a', 'a', '2', '2014-12-13', '15 : 22 : '),
(5, 3, 13, 'b', 'b', 'b', '1', '2014-12-12', '15 : 24 : ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudservicio`
--

CREATE TABLE IF NOT EXISTS `solicitudservicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFuncionario` int(11) NOT NULL,
  `coordinacion` varchar(100) NOT NULL,
  `departamento` varchar(100) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `codigodeequipo` varchar(50) NOT NULL,
  `tipoequipo` varchar(50) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `codigobienes` varchar(50) NOT NULL,
  `tipodefalla` varchar(100) NOT NULL,
  `descripciondefalla` varchar(100) NOT NULL,
  `fechadeingreso` date NOT NULL,
  `hora` varchar(20) NOT NULL,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `solicitudservicio`
--

INSERT INTO `solicitudservicio` (`id`, `idFuncionario`, `coordinacion`, `departamento`, `ubicacion`, `telefono`, `codigodeequipo`, `tipoequipo`, `serial`, `codigobienes`, `tipodefalla`, `descripciondefalla`, `fechadeingreso`, `hora`, `estado`) VALUES
(1, 20, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', '3', '1', '2014-10-01', '', '3'),
(2, 20, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', '3', '1', '2014-11-06', '', '3'),
(7, 20, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', '3', '3', '2014-12-07', '       13 :', '3'),
(8, 20, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 1', '2014-12-07', '16 : 28 : 2', '3'),
(9, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 1', '2014-12-10', '19 : 29 : 4', '3'),
(10, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 1', '2014-12-10', '19 : 31 : 1', '3'),
(11, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 1', '2014-12-10', '19 : 32 : 0', '3'),
(12, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 3', 'DESCRIPCION FALLA 3', '2014-12-10', '19 : 41 : 4', '3'),
(13, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 1', '2014-12-11', '21 : 04 : 4', '4'),
(14, 2, 'coordinacion ', 'departamento', 'ubicacion', 'telefono', 'cde0000', 'pc', 's00000', 'cb00000', 'FALLA 1', 'DESCRIPCION FALLA 3', '2014-12-11', '21 : 10 : 1', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `cedula` varchar(9) NOT NULL,
  `cargo` varchar(50) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `unidadgeneral` varchar(50) NOT NULL,
  `subunidadgeneral` varchar(50) NOT NULL,
  `unidad` varchar(50) NOT NULL,
  `unidadgeneral2` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `cedula`, `cargo`, `tipo`, `unidadgeneral`, `subunidadgeneral`, `unidad`, `unidadgeneral2`, `usuario`, `clave`) VALUES
(1, 'Javier', '19940338', 'Gerente', 'Administrador', '1', '1', '', '1', 'cheche', '123'),
(2, 'Jose', '19940330', 'gerente', 'Usuario General', '1', '1', '1unidad', '1', 'cheche1', '123'),
(3, 'carlos', '19940331', 'gerente', 'Tecnico', '1', '1', '1unidad', '1', 'tecnico', '123'),
(5, 'luis', '19940228', 'gerente', 'Tecnico', '2', '2', '2unidad', '2', 'tecnico2', '123');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
