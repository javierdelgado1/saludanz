<div class="row">
            <div class="col-xs-12">
                 <ol class="breadcrumb">
                    <li id="menuprincipal"><a href="#">Menu Principal</a>
                    </li>
                    <li class="active">Solicitudes Realizadas</li>
                </ol>
            </div>
    </div>

    <br>
    <div id="alertasSolicitud"></div>
    <table style="width:100%;" class="well">
                    <tr>
                        <td><b> Fecha:</b> Desde:</td>
                        <td><input type="date" class="form-control" id="Fechadesde1"></td>
                        <td>Hasta:</td>
                        <td><input type="date" class="form-control" id="FechaHasta1"></td>
                        
                        <td><button class="btn btn-default btn-xs" type="button" id="buscar1"><span class="glyphicon glyphicon-search"></span></button></td>
                    </tr>
                </table>
    <div class="alinearizquierda2">
	    <input type="checkbox" id="noA" checked="true">
	    <label >No Asignado</label>
	    <input type="checkbox" id="A">
	    <label >Asignado</label>
	    <input type="checkbox" id="S">
	    <label >Solucionado</label>
	    <input type="checkbox" id="noS">
	    <label >No Solucionado</label>
	</div>

    <div class="alinearizquierda2" id="resultadonumero"></div>
    
    <div class="datagrid" id="listado">
        <table style="width:100%;" class="table hover">
        	<thead>
        		<tr>
                    <th>
                        
                    </th>
        			<th>
        				Nº DE SOLICITUD
        			</th>
        			<th>
        				FECHA
        			</th>
        			<th>
        				HORA
        			</th>
        			<th>
        				UNIDAD
        			</th>
                    <th>
                        TIPO DE FALLA
                    </th>
                    <th>
                        ESTATUS
                    </th>
        		</tr>
        	</thead>
            <!-- <tbody>
                <tr>   
                    <td></td>
                    <td>00000001</td>
                    <td>12/12/14</td>
                    <td>7:00PM</td>
                    <td>asdasd</td>
                    <td>asdsad</td>
                    <td>Reportada</td>
                </tr>
            </tbody> -->
        </table>
    </div>

       <script type="text/javascript"> 
        $(document).ready(function() {

       validarcheckbos();
           
        buscarporestado(1);
         function buscarporestado(estado){
            $('#buscar1').on("click", function() {
            if(Fechadesde1.value!=""&&FechaHasta1.value!=""){
                console.log("Fecha Desde: " + Fechadesde1.value + " Fecha Hasta: "+FechaHasta1.value);
         	 $.ajax
                ({
                type: "POST",
                url: "modelo/consultasServicio.php",
                data: {id:5, estado:estado, desde:Fechadesde1.value, hasta:FechaHasta1.value },
                async: false,   
                dataType: "json",       
                success:
                function (msg) 
                {   
                   /* console.log(msg);*/
                    var table="";
                    var row="";
                    if(msg[0].m>0){
						$('#listado').html("");
						table = $('<table style="width:100%;" class="table"></table>');                         
						row=$('<thead></thead>').append("<tr></tr>");
						$('#listado').append(ListarTabla(msg, table, row));          
                    }
                    else{
						$('#listado').html("");
						table = $('<table style="width:100%;" class="table"></table>');                         
						row=$('<thead></thead>').append("<tr></tr>");
						row.append($('<th></th>').html(""));
						row.append($('<th></th>').html("<b>Nº DE SOLICITUD</b>"));                            
						row.append($('<th></th>').html("<b>FECHA</b>"));
						row.append($('<th></th>').html("<b>HORA</b>"));
						row.append($('<th></th>').html("<b>TIPO DE FALLA</b>"));
						row.append($('<th></th>').html("<b>ESTATUS</b>"));
                        resultadonumero.innerHTML="Mostrando <span class='badge'>"+msg[0].m+"</span> Resultados";

						table.append(row);  
						var row2 = $('<tbody></tbody>');

						table.append(row2);
						$('#listado').append(table); 
                    }
                 },
                    error:
                    function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                });
            }
            else{
                alertasSolicitud.innerHTML='<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Llene todas las fechas por favor!</div>';
            }
        });
         }
        function validarcheckbos(){
        	/*if($('#noS').checked){
				console.log("No Asginado Activado");
			}
			else{
				console.log("No Asginado DesActivado");

			}*/
			$('#noS').on('click',  function(){
				if ($('input[id="noS"]').is(':checked'))
				{	console.log("checkbox No Solucionado Activado");
					A.checked=false;
					noA.checked=false;
					S.checked=false;
        		   buscarporestado(4);

				}				
			});
			$('#S').on('click',  function(){
				if ($('input[id="S"]').is(':checked'))
				
				{	console.log("checkbox Solucionado Activado");

					A.checked=false;
					noA.checked=false;
					noS.checked=false;
       			 buscarporestado(3);

				}				
			});
			$('#A').on('click',  function(){
				if ($('input[id="A"]').is(':checked'))
				{
				    console.log("checkbox ASGINADO Activado");

					S.checked=false;
					noA.checked=false;
					noS.checked=false;
        			buscarporestado(2);

				}				
			});
			$('#noA').on('click',  function(){
				if ($('input[id="noA"]').is(':checked'))
				
				{	console.log("checkbox NO ASGINADO Activado");

					S.checked=false;
					A.checked=false;
					noS.checked=false;
       				 buscarporestado(1);

				}				
			});
        }
        
        function ListarTabla(msg, table, row){         
                row.append($('<th></th>').html(""));
                row.append($('<th></th>').html("<b>Nº DE SOLICITUD</b>"));                            
                row.append($('<th></th>').html("<b>FECHA</b>"));
                row.append($('<th></th>').html("<b>HORA</b>"));
                row.append($('<th></th>').html("<b>TIPO DE FALLA</b>"));
                row.append($('<th></th>').html("<b>ESTATUS</b>"));
                table.append(row);  
                var row2 = $('<tbody></tbody>');
                resultadonumero.innerHTML="Mostrando <span class='badge'>"+msg[0].m+"</span> Resultados";

                for(i=0; i<msg[0].m; i++){
                   
                        var row3=$('<tr></tr>');
                        var fila0=$('<td></td>').text((i+1));
                        var fila1 = $('<td></td>').text(msg[i].id);
                        var fila2 = $('<td></td>').text(msg[i].fechadeingreso);
                        var fila3 = $('<td></td>').text(msg[i].hora);
                        var fila4 = $('<td></td>').text(msg[i].tipodefalla);
                        var fila5="";
                        if(msg[i].estatus=="1")
                        var fila5 = $('<td></td>').text("No Asignado");
                        if(msg[i].estatus=="2")
                        var fila5 = $('<td></td>').text("Asignado");
                        if(msg[i].estatus=="3")
                        var fila5 = $('<td></td>').text("Solucionado");
                        if(msg[i].estatus=="4")
                        var fila5 = $('<td></td>').text("No Solucionado");                    
                         row3.append(fila0);
                        row3.append(fila1);
                        row3.append(fila2);
                        row3.append(fila3);
                        row3.append(fila4);  
                        row3.append(fila5); 
                        row2.append(row3);                              
                       
                    
                     
                }
                table.append(row2);
                return table;
            }

        });
    </script>