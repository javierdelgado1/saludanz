<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

<style type="text/css">
    .btn {
            padding: 2px 12px;
        }
    .input-group-addon {
        padding: 2px 12px;
        }
</style>
        <div class="row">
            <div class="col-xs-12">
                 <ol class="breadcrumb">
                    <li id="menuprincipal"><a href="#">Menu Principal</a>
                    </li>
                    <li class="active">Solicitud de Servicio</li>
                </ol>
            </div>
    </div>
         <div class="row">
            <div class="col-xs-4">
                    <div class="form-group">
                        <label >Numero de solicitud</label>
                        <input type="text" class="form-control input-sm" id="tNumeroSolicitud">
                    </div>
            </div>
             <div class="col-xs-1">
                    
            </div>
             <div class="col-xs-3">
                   
            </div>
           <div class="col-xs-4">
                <div class="form-group">
                        <label >Fecha de Ingreso</label>
                        <input type="date" class="form-control input-sm " id="Fecha" value="<?php echo date('Y-m-d'); ?>">
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                   
            </div>
             <div class="col-xs-2">
                    
            </div>
             <div class="col-xs-3">
                   
            </div>
           <div class="col-xs-4">
                <div class="form-group">
                        <label >Hora de inicio</label>
                        <input type="text" class="form-control input-sm" id="reloj" name="reloj">
                </div>
                <div class="form-group">
                        <button type="button" class="btn btn-default btn-xs" id="enviar">ENVIAR</button>
                </div>
            </div>
        </div>
       
             <div class="row">
                    <div class="col-xs-1">
                    </div>
                    <div class="col-xs-10">
                            <div id="alertas5"></div>
                    </div>
                    <!-- <div class="col-xs-1">
                    </div>     -->      
             </div> 
     <!--         <br>
            <h5><b>DATOS DEL FUNCIONARIO</b></h5>
            <hr>
            <div class="row">
                 <div class="col-xs-4">
                    CEDULA
                </div>
                <div class="col-xs-4">
                    NOMBRE
                </div>
                <div class="col-xs-4">
                     CARGO
                </div>
            </div>
            <div class="row">
                 <div class="col-xs-4">
                    <div class="input-group">
                      <input type="text" class="form-control input-sm" id="tcedula">
                      <span class="input-group-btn">
                        <button class="btn btn-default btn-xs" type="button" id="buscarFuncionario"><span class="glyphicon glyphicon-search"></span></button>
                      </span>
                    </div>
                </div>
                <div class="col-xs-4">
                    <input type="text" class="form-control input-sm" id="tnombre">
                </div>
                <div class="col-xs-4">
                     <input type="text" class="form-control input-sm" id="tcargo">
                </div>
            </div>
                
            </div>
            <br> -->
            <h5><b>DATOS DE UBICACION</b></h5>
            <hr>
            <div class="datagrid">
                 <table style="width:100%;" class="table hover">
    
            <thead>
                <tr>
                    <th>
                        COORDINACION
                    </th>
                    <th>
                        DEPARTAMENTO
                    </th>
                    <th>
                        UBICACION
                    </th>
                    <th>
                        TELEFONO
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" class="form-control input-sm " id="tcoordinacion" >
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" id="tdepartamento" >
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" id="tubicacion" >
                    </td>
                    <td>
                        <div class="form-inline">
                            <select id="operadora" style="width:30%;">
                                <option value="0414">0414</option>
                                <option value="0424">0424</option>
                                <option value="0416">0416</option>
                                <option value="0426">0426</option>
                                <option value="0412">0412</option>
                            </select>                     
                            <input type="text" class="form-control input-sm" id="tTelefono" style="width:65%;">
                        </div>
                    </td>
                   
                </tr>
            </tbody>
            </table>
            <br>
            <h5><b>DATOS DEL EQUIPO</b></h5>
            <hr>
            <table style="width:100%;">
            <thead>
                <tr>
                    <th>
                        CODIGO DE EQUIPO
                    </th>
                    <th>
                        TIPO DE EQUIPO
                    </th>
                    <th>
                        SERIAL
                    </th>
                    <th>
                        CODIGO BIENES
                    </th>
                </tr>
            </thead>
                <tr>
                    <td>
                        <select class="form-control input-sm"  id="tcodigoequipo">
                            <option value="-1">Seleccione Aqui</option>
                        </select>
                       <!--  <input type="text" class="form-control input-sm" id="tcodigoequipo" > -->
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" id="ttipoequipo" readonly >
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" id="tserial" readonly>
                    </td>
                    <td>
                        <input type="text" class="form-control input-sm" id="tcodigobienes" readonly>
                    </td>
                </tr>
            </table>
            <br>
        <h5><b>REPORTE DE FALLAS</b></h5>
            <hr >
            <br>
            <div style="text-align:left;">
               <div class="form-inline">
                            <label >TIPO DE FALLA</label>
                            <select class="form-control input-sm" id="tipodefalla">
                                <option value="-1"></option>
                                <option >FALLA 1 </option>
                                <option >FALLA 2</option>
                                <option >FALLA 3</option>
                            </select>
                </div>
                <br>
                <div class="form-inline">
                            <label >DESCRIPCION DE FALLA</label>
                            <select class="form-control input-sm"  id="descripcionfalla">
                                <option value="-1"></option>
                                <option >DESCRIPCION FALLA 1 </option>
                                <option >DESCRIPCION FALLA 2</option>
                                <option >DESCRIPCION FALLA 3</option>
                            </select>
                </div>
            </div>
        
    <script type="text/javascript"> 
    $(document).ready(function() {
        $( "#tcodigoequipo" ).change(function() {
              $.ajax
                ({
                type: "POST",
                url: "modelo/consultasEquipo.php",
                data: {id:6, idEquipo:tcodigoequipo.value},
                async: false,   
                dataType: "json",   
                success:
                function (msg) 
                {   
                    ttipoequipo.value=msg[0].tipo;
                    tserial.value=msg[0].serial;
                    tcodigobienes.value=msg[0].codigobienes;
                 },
                    error:
                    function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                });
        });
        NUmerodesolicitud();
        function NUmerodesolicitud(){ 
                $.ajax
                    ({
                    type: "POST",
                    url: "modelo/consultasServicio.php",
                    data: {id:2},
                    async: false,   
                    dataType: "json",   
                    success:
                    function (msg) 
                    {   
                        tNumeroSolicitud.value=msg;
                     },
                        error:
                        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                    });

    }

                $.ajax
                    ({
                    type: "POST",
                    url: "modelo/consultasEquipo.php",
                    data: {id:5},
                    async: false,   
                    dataType: "json",   
                    success:
                    function (msg) 
                    {   
                               
                        for(i=0; i<msg[0].m; i++)
                            {
                                
                                 tcodigoequipo.options[i+1]= new Option (msg[i].codigoequipo + "  " + msg[i].tipo);
                                tcodigoequipo.options[i+1].text =msg[i].codigoequipo + "  " + msg[i].tipo;
                                tcodigoequipo.options[i+1].value =msg[i].id;   

                            }
                     },
                        error:
                        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                    });

        $('#tcodigoequipo').multiselect({
                    enableFiltering: true,
                    filterPlaceholder: 'Buscar'               
         
         });

        $('#enviar').on("click", function() {
            console.log(operadora.value);
            if (validarRegistro()) {
                alertas5.innerHTML="";
                console.log("Fecha de ingreso: "+Fecha.value + " Reloj: "+$('#reloj').val());
                tTelefono.value=operadora.value+tTelefono.value;
                $.ajax
                    ({
                    type: "POST",
                    url: "modelo/consultasServicio.php",
                    data: {id:1, coordinacion:tcoordinacion.value, departamento:tdepartamento.value, ubicacion:tubicacion.value, telefono:tTelefono.value, codigodeequipo:tcodigoequipo.value, tipodefalla:tipodefalla.value, descripcionfalla:descripcionfalla.value, fecha:Fecha.value, reloj:$('#reloj').val()},
                    async: false,   
                    dataType: "json",   
                    success:
                    function (msg) 
                    {   console.log(msg);
                        if(msg=="false"){
                            alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El nombre de usuario o la cedula ya existe por favor escriba otro!</div>';
                        }
                        else{
                            limpiar();
                            NUmerodesolicitud();
                            alertas5.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Se ha registrado correctamente!</div>';
                        }
                     },
                        error:
                        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                    });

            }
        });
      /*  $('#buscarFuncionario').on("click", function() {
            console.log(validarBusquedadeCedula());
            if (validarBusquedadeCedula()) {
                alertas5.innerHTML="";
                console.log("Cedula a buscar "+tcedula.value);
                $.ajax
                    ({
                    type: "POST",
                    url: "modelo/consultasUsuario.php",
                    data: {id:7, cedula:tcedula.value},
                    async: false,   
                    dataType: "json",   
                    success:
                    function (msg) 
                    {   console.log(msg[0].m);
                        if(msg[0].m>0){
                            alertas5.innerHTML="";
                            console.log("Nombre: "+msg[0].nombre);
                            tnombre.value=msg[0].nombre;
                            tcargo.value=msg[0].cargo;

                            
                            
                        }
                        else{
                            alertas5.innerHTML='<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Cedula no encontrada!</div>';
                        }
                        
                     },
                        error:
                        function (msg) {console.log( msg +"No se pudo realizar la conexion");}
                    });


            }
        });*/

        function limpiar(){
            
           /* tnombre.value="";
            tcargo.value="";*/
            tcoordinacion.value="";
            tdepartamento.value="";
            tubicacion.value="";
            tTelefono.value="";
            tcodigoequipo.value="";
            ttipoequipo.value="";
            tserial.value="";
            tcodigobienes.value="";
            $('#tipodefalla').val(0);
            $('#descripcionfalla').val(0);
        }
        function validarBusquedadeCedula(){
            if(!tcedula.value.match(/^[0-9]+$/)){
                alertas5.innerHTML='<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor inicie la busqueda del funcionario colocando la <b>Cedula</b>!</div>';
                return false;
            }
            else {return true;}
        }
        function validarRegistro(){
           /* if(!tcedula.value.match(/^[0-9]+$/))
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>cedula</b> con numeros solamente!</div>';
                return false;
            }
            else if(tnombre.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Nombre</b>!</div>';
                return false;
            }       
            
            else if(tcargo.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Cargo</b>!</div>';
                return false;
            }
            */
            
            if(tcoordinacion.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>Coordinacion</b>!</div>';
                return false;
            }
            else if(tdepartamento.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo<b>DEPARTAMENTO</b>!</div>';
                return false;
            }
            else if(tubicacion.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo<b>UBICACION</b>!</div>';
                return false;
            }
            else if(tTelefono.value==""&&!tTelefono.value.match(/^[0-9]+$/)||(tTelefono.value).length<7)
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo<b>TELEFONO</b>, Solo numeros, deben ser 7!</div>';
                return false;
            }
            else if(tcodigoequipo.value=="")
            {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>CODIGO DE EQUIPO</b>!</div>';
                return false;
            }
            else if(ttipoequipo.value=="")
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>TIPO DE EQUIPO</b>!</div>';
                
                return false;
            }
            else if(tserial.value=="")
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>SERIAL</b>!</div>';
                
                return false;
            }
            else if(tcodigobienes.value=="")
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor llene el campo <b>CODIGO  BIENES</b>!</div>';
                
                return false;
            }
            else if(tipodefalla.value=="-1")
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor seleccione un <b>TIPO  DE FALLA</b>!</div>';
                
                return false;
            }
            else if(descripcionfalla.value=="-1")
                        {  
                alertas5.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Por Favor seleccione una <b>DESCRIPCION DE FALLA</b>!</div>';
                
                return false;
            }
            else {return true;}
        }
    });
</script>


     
    