<?php
	include("conector.php");
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			RegistrarEquipo();
			break;
		case 2:
			BuscarEquipo();
			break;
		case 3:
			modificarEquipo();
			break;
		case 4:
			EliminarEquipo();
			break;
		case 5:
			ObtenerEquipos();
			break;
		case 6:
			ObtenerEquipoEspecifico();
			break;
		default;
	}

	function ObtenerEquipoEspecifico(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$id=$_REQUEST['idEquipo'];
		$tupla="SELECT * FROM  equipo  WHERE  idEquipo='$id'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['idEquipo'];
			$objeto[$i]['codigoequipo']=$db_resultado['codigoequipo'];
			$objeto[$i]['tipo']=$db_resultado['tipo'];
			$objeto[$i]['serial']=$db_resultado['serial'];
			$objeto[$i]['codigobienes']=$db_resultado['codigobienes'];		
		}
		$mysqli->close();
		echo json_encode($objeto);		

	}
	function ObtenerEquipos(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);		
		$tupla="SELECT idEquipo, codigoequipo, tipo FROM  equipo  ORDER BY  idEquipo DESC";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['idEquipo'];
			$objeto[$i]['codigoequipo']=$db_resultado['codigoequipo'];
			$objeto[$i]['tipo']=$db_resultado['tipo'];

			
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function EliminarEquipo(){

		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$salida="true";
		session_start();		
		$idEquipo=$_SESSION['idEquipo'];		
		$tupla="DELETE FROM  equipo WHERE idEquipo='$idEquipo'";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
		$mysqli->close();
		echo json_encode($salida);
	
	}
	function modificarEquipo(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$codigoequipo=$_REQUEST['codigoequipo'];
		$serial=$_REQUEST['serial'];
		$codigobienes=$_REQUEST['codigobienes'];
		$tipo=$_REQUEST['tipo'];
		$idEquipo=$_SESSION['idEquipo'];
		$salida="true";		
		$tupla="UPDATE equipo SET codigoequipo='$codigoequipo', serial='$serial', codigobienes='$codigobienes', tipo='$tipo' WHERE  idEquipo='$idEquipo'";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
		$a='serial';
		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="1";
		} 
		$a='codigobienes';

		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="2";
		} 
		$a='codigoequipo';

		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="3";
		} 

		$mysqli->close();
		echo json_encode($salida);
	}
	function BuscarEquipo(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$cedula=$_REQUEST['cedula'];
		$tupla="SELECT * FROM  equipo  WHERE  serial='$cedula'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			session_start();
			$objeto[0]['idEquipo']=$db_resultado['idEquipo'];
			$_SESSION['idEquipo']=$objeto[0]['idEquipo'];
			$objeto[0]['codigoequipo']=$db_resultado['codigoequipo'];
			$objeto[0]['tipo']=$db_resultado['tipo'];
			$objeto[0]['serial']=$db_resultado['serial'];
			$objeto[0]['codigobienes']=$db_resultado['codigobienes'];		
			
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function RegistrarEquipo(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$codigoequipo=$_REQUEST['codigoequipo'];
		$serial=$_REQUEST['serial'];
		$codigobienes=$_REQUEST['codigobienes'];
		$tipo=$_REQUEST['tipo'];
		$salida="true";
		$tupla="INSERT INTO  equipo (codigoequipo, serial, codigobienes, tipo) VALUES  ('$codigoequipo', '$serial', '$codigobienes', '$tipo')";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;

		$a='serial';
		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="1";
		} 
		$a='codigobienes';

		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="2";
		} 
		$a='codigoequipo';

		$pos=strpos($salida,$a);
		if ($pos!=false) {
			$salida="3";
		} 

		echo json_encode($salida);
	}