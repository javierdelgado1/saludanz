<?php
	include("conector.php");
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			Solicituddeservicio();
			break;
		case 2:
			ObtenernumerodeSolicitud();
			break;
		case 3:
			BuscarPorMesyAno();
			break;
		case 4:
			BuscarSolicitudes();
			break;
		case 5:
			BuscarSolicitudesporEstado();
			break;
		case 6:
			ObtenerServicio();
			break;
		case 7:
			AsignarTecnico();
			break;
		case 8:
			obtenerASignadosyNoASginados();
			break;
		case 9:
			BuscarAsignacion();
			break;
		case 10:
			ConsultasdesolicitudTecnico();
			break;
		case 11:
			ObtenerTenicos();
			break;
		case 12:
			ConsultasdesolicitudTecnicoPorTecnico();
			break;
		default;
	}
	function ConsultasdesolicitudTecnicoPorTecnico(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$idTecnico=$_REQUEST['idTecnico'];
		$tupla="SELECT usuario.nombre,  solicitudservicio.* FROM  asignaciones INNER JOIN usuario on  asignaciones.idusuario=usuario.id INNER JOIN solicitudservicio on solicitudservicio.id=asignaciones.idsolicitud WHERE  asignaciones.idusuario='$idTecnico' ORDER BY   asignaciones.id DESC";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];

			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');


			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$id=$db_resultado['idFuncionario'];
			$tupla2="SELECT nombre FROM  usuario WHERE  id='$id'";
			$resultado2 = $mysqli->query($tupla2);
			
			if($db_resultado2 = mysqli_fetch_array($resultado2, MYSQLI_ASSOC))
			{

				$objeto[$i]['reportadopor']=$db_resultado2['nombre'];
			}
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function ObtenerTenicos(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		
		$tupla="SELECT  id, nombre, cedula  FROM usuario WHERE  tipo='Tecnico' ORDER BY id DESC";
		
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			$objeto[$i]['nombre']=$db_resultado['nombre'];
			$objeto[$i]['cedula']=$db_resultado['cedula'];

			
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function ConsultasdesolicitudTecnico(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$idTecnico=$_SESSION['id'];
		$tupla="SELECT usuario.nombre,  solicitudservicio.* FROM  asignaciones INNER JOIN usuario on  asignaciones.idusuario=usuario.id INNER JOIN solicitudservicio on solicitudservicio.id=asignaciones.idsolicitud WHERE  asignaciones.idusuario='$idTecnico' AND  solicitudservicio.estado!='3' ORDER BY  solicitudservicio.id DESC";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];

			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');


			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$id=$db_resultado['idFuncionario'];
			$tupla2="SELECT nombre FROM  usuario WHERE  id='$id'";
			$resultado2 = $mysqli->query($tupla2);
			
			if($db_resultado2 = mysqli_fetch_array($resultado2, MYSQLI_ASSOC))
			{

				$objeto[$i]['reportadopor']=$db_resultado2['nombre'];
			}
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function BuscarAsignacion(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$idsolicitud=$_REQUEST['idsolicitud'];
		$tupla="SELECT  asignaciones.idusuario as id, usuario.nombre as nombre FROM  asignaciones  INNER JOIN solicitudservicio on asignaciones.idsolicitud=solicitudservicio.id INNER JOIN  usuario on usuario.id=asignaciones.idusuario WHERE  solicitudservicio.id='$idsolicitud'";
		$resultado = $mysqli->query($tupla);
		$i=0;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			/*while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}*/
			$objeto[$i]['nombre']=$db_resultado['nombre'];
			
		}
		$mysqli->close();
		echo json_encode($objeto);

	}
	function obtenerASignadosyNoASginados(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		
		$tupla="SELECT  fechadeingreso, hora, tipodefalla, estado, id FROM solicitudservicio WHERE  estado='1' or estado='2'  ORDER BY id DESC";
		
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');

			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
		/*	$objeto[$i]['unidad']=$db_resultado['unidad'];*/
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function AsignarTecnico(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$idsolicitud=$_REQUEST['idsolicitud'];
		$tecnico=$_REQUEST['tecnico'];
		$salida="true";
		$mysqli->query("DELETE FROM asignaciones WHERE  idsolicitud='$idsolicitud'");
		$tupla="INSERT INTO asignaciones (idusuario, idsolicitud) VALUES ('$tecnico', '$idsolicitud')";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
		if($salida=="true"){
			$tupla="UPDATE  solicitudservicio SET  estado='2' WHERE  id='$idsolicitud'";
			$resultado = $mysqli->query($tupla);

		}
		$mysqli->close();
		echo json_encode($salida);
	}
	function ObtenerServicio(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$id=$_REQUEST['idservicio'];
		$tupla="SELECT  solicitudservicio.*,  equipo.codigoequipo, equipo.tipo as tipoEquipo, equipo.serial, equipo.codigobienes, usuario.* FROM solicitudservicio inner join usuario on solicitudservicio.idFuncionario=usuario.id  inner join  equipo on equipo.idEquipo=solicitudservicio.idEquipo WHERE  solicitudservicio.id='$id'";
		
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		
		$i=0;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$id;
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			
			$objeto[$i]['coordinacion']=$db_resultado['coordinacion'];
			$objeto[$i]['departamento']=$db_resultado['departamento'];
			$objeto[$i]['ubicacion']=$db_resultado['ubicacion'];
			$objeto[$i]['telefono']=$db_resultado['telefono'];
			$objeto[$i]['codigodeequipo']=$db_resultado['codigoequipo'];
			$objeto[$i]['tipoequipo']=$db_resultado['tipoEquipo'];
			$objeto[$i]['serial']=$db_resultado['serial'];
			$objeto[$i]['codigobienes']=$db_resultado['codigobienes'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$objeto[$i]['descripciondefalla']=$db_resultado['descripciondefalla'];

			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');
			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$objeto[$i]['reportadopor']=$db_resultado['nombre'];
			$objeto[$i]['telefono']=$db_resultado['telefono'];


			$objeto[$i]['unidadgeneral']=$db_resultado['unidadgeneral'];
			$objeto[$i]['subunidadgeneral']=$db_resultado['subunidadgeneral'];
			$objeto[$i]['unidad']=$db_resultado['unidad'];
			$objeto[$i]['unidadgeneral2']=$db_resultado['unidadgeneral2'];


		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function BuscarSolicitudesporEstado(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$id=$_SESSION['id'];
		$estado=$_REQUEST['estado'];
		$desde=$_REQUEST['desde'];
		$hasta=$_REQUEST['hasta'];
		$tupla="SELECT  fechadeingreso, hora, tipodefalla, estado, id FROM solicitudservicio WHERE  estado='$estado' AND  fechadeingreso BETWEEN '$desde'  AND '$hasta' ORDER BY  id DESC";
		
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');

			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
		/*	$objeto[$i]['unidad']=$db_resultado['unidad'];*/
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function BuscarSolicitudes(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$id=$_SESSION['id'];
		$tupla="SELECT  fechadeingreso, hora, tipodefalla, estado, id FROM solicitudservicio WHERE  idFuncionario='$id' ORDER BY  id DESC";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');
			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
		/*	$objeto[$i]['unidad']=$db_resultado['unidad'];*/
			$objeto[$i]['estatus']=$db_resultado['estado'];
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function BuscarPorMesyAno(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$mes=$_REQUEST['mes'];
		$ano=$_REQUEST['ano'];
		$tupla="SELECT id, fechadeingreso, hora, tipodefalla FROM solicitudservicio WHERE MONTH(fechadeingreso)='$mes'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			while(strlen($objeto[$i]['id'])<7){
				$objeto[$i]['id']="0".$objeto[$i]['id'];
			}
			$objeto[$i]['fechadeingreso']=explode("-", $db_resultado['fechadeingreso']);
			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);

	}
	function ObtenernumerodeSolicitud(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$tupla="SELECT id as total FROM `solicitudservicio` ORDER BY `solicitudservicio`.`id` DESC limit 0,1";
		$resultado = $mysqli->query($tupla);
		$total="";
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			
			$total=$db_resultado['total']+1;		
			while (strlen($total)<7) {
				$total="0".$total;
			}
		}
		$mysqli->close();
		echo json_encode($total);
	}
	function Solicituddeservicio(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$coordinacion=$_REQUEST['coordinacion'];
		$departamento=$_REQUEST['departamento'];
		$ubicacion=$_REQUEST['ubicacion'];
		$telefono=$_REQUEST['telefono'];
		$codigodeequipo=$_REQUEST['codigodeequipo'];
		/*$tipoequipo=$_REQUEST['tipodeequipo'];
		$serial=$_REQUEST['serial'];
		$codigobienes=$_REQUEST['codigodebienes'];*/
		$tipodefalla=$_REQUEST['tipodefalla'];
		$descripciondefalla=$_REQUEST['descripcionfalla'];
		/*$cedula=$_REQUEST['cedula'];*/
		$fecha=$_REQUEST['fecha'];
		$reloj=$_REQUEST['reloj'];
		$id=$_SESSION['id'];
		$salida="true";
		/*$tupla="SELECT id FROM  usuario  WHERE  cedu='$cedula'";
		$resultado = $mysqli->query($tupla);
		$idFuncionario="";
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			
			$idFuncionario=$db_resultado['id']+1;	*/	
			$tupla="INSERT INTO solicitudservicio (idFuncionario, coordinacion, departamento, ubicacion, telefono, idEquipo,  tipodefalla, descripciondefalla, fechadeingreso, hora, estado)  VALUES ('$id', '$coordinacion', '$departamento','$ubicacion','$telefono','$codigodeequipo','$tipodefalla','$descripciondefalla', '$fecha', '$reloj', '1')";
			$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
				
		/*}*/
		/*else{
			$salida="false";
		}*/


			$mysqli->close();
			echo json_encode($salida);
		

	}
?>