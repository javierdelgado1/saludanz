<?php
	include "conector.php";
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			iniciarsesion($_REQUEST['temail'], $_REQUEST['tpass']);
			break;
		case 2:
			verificarLogeo();
			break;
		case 3:
			cerrarSesion();
			break;
		case 4:
			Agregar();
			break; 
		case 5:
			Buscar();
			break;
		case 6:
			RegistrarFuncionario();
			break;
		case 7:
			BuscarFuncionario();
			break;
		case 8:
			ModificarFuncionario();
			break;

		case 9:
			EliminarFuncionario();
			break;
		case 10:
			ObtenerTecnicos();
			break;

		default;
	}
	function ObtenerTecnicos(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		
		
		$tupla="SELECT id, nombre, cedula FROM  usuario WHERE  tipo='Tecnico'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['id']=$db_resultado['id'];
			$objeto[$i]['nombre']=$db_resultado['nombre'];
			$objeto[$i]['cedula']=$db_resultado['cedula'];
			/*$objeto[0]['cargo']=$db_resultado['cargo'];
			
			$objeto[0]['unidadgeneral']=$db_resultado['unidadgeneral'];
			$objeto[0]['subunidadgeneral']=$db_resultado['subunidadgeneral'];
			$objeto[0]['unidad']=$db_resultado['unidad'];
			$objeto[0]['unidadgeneral2']=$db_resultado['unidadgeneral2'];
			$objeto[0]['usuario']=$db_resultado['usuario'];
			$objeto[0]['clave']=$db_resultado['clave'];
			$objeto[0]['tipo']=$db_resultado['tipo'];	*/
			$i++;
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function EliminarFuncionario(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$usuario=$_REQUEST['usuario'];
		$salida="true";
		
		$tupla="DELETE FROM  usuario  WHERE usuario='$usuario'";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
		$mysqli->close();
		echo json_encode($salida);
	}
	function ModificarFuncionario(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$cedula=$_REQUEST['cedula'];
		$nombre=$_REQUEST['nombre'];
		$cargo=$_REQUEST['cargo'];		
		$usuario=$_REQUEST['usuario'];
		$clave=$_REQUEST['clave'];
		$unidadgeneral=$_REQUEST['unidadgeneral'];
		$subunidadgeneral=$_REQUEST['subunidadgeneral'];
		$unidad=$_REQUEST['unidad'];
		$unidadgeneral2=$_REQUEST['unidadgeneral2'];
		$tipo=$_REQUEST['tipo'];
		$ID=$_SESSION['id'];
		$_SESSION['tipo']=$tipo;
		$salida="true";
		$tupla="UPDATE usuario SET cedula='$cedula', nombre='$nombre', cargo='$cargo', clave='$clave', unidadgeneral='$unidadgeneral', subunidadgeneral='$subunidadgeneral', unidad='$unidad', unidadgeneral2='$unidadgeneral2',  tipo='$tipo' WHERE  usuario='$usuario'";
		$resultado = $mysqli->query($tupla) or $salida=$mysqli->error;
		$mysqli->close();
		echo json_encode($salida);
	}
	function BuscarFuncionario(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$cedula=$_REQUEST['cedula'];
		$tupla="SELECT * FROM  usuario  WHERE  cedula='$cedula'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[0]['id']=$db_resultado['id'];
			$objeto[0]['nombre']=$db_resultado['nombre'];
			$objeto[0]['cedula']=$db_resultado['cedula'];
			$objeto[0]['cargo']=$db_resultado['cargo'];
			
			$objeto[0]['unidadgeneral']=$db_resultado['unidadgeneral'];
			$objeto[0]['subunidadgeneral']=$db_resultado['subunidadgeneral'];
			$objeto[0]['unidad']=$db_resultado['unidad'];
			$objeto[0]['unidadgeneral2']=$db_resultado['unidadgeneral2'];
			$objeto[0]['usuario']=$db_resultado['usuario'];
			$objeto[0]['clave']=$db_resultado['clave'];
			$objeto[0]['tipo']=$db_resultado['tipo'];	
			
		}
		$mysqli->close();
		echo json_encode($objeto);
	}
	function RegistrarFuncionario(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$cedula=$_REQUEST['cedula'];
		$nombre=$_REQUEST['nombre'];
		$cargo=$_REQUEST['cargo'];		
		$usuario=$_REQUEST['usuario'];
		$clave=$_REQUEST['clave'];
		$unidadgeneral=$_REQUEST['unidadgeneral'];
		$subunidadgeneral=$_REQUEST['subunidadgeneral'];
		$unidad=$_REQUEST['unidad'];
		$unidadgeneral2=$_REQUEST['unidadgeneral'];
		$tipo=$_REQUEST['tipo'];
		$salida="true";
		$tupla="INSERT INTO usuario (usuario, clave, nombre, cedula, cargo, tipo, unidadgeneral, unidad, subunidadgeneral, unidadgeneral2) 
				VALUES ('$usuario', '$clave', '$nombre', '$cedula', '$cargo', '$tipo', '$unidadgeneral', '$unidad', '$subunidadgeneral', '$unidadgeneral2')";
		$resultado = $mysqli->query($tupla) or  $salida=$mysqli->error;

		if($salida!="true"&&$salida[0]=="D"&&$salida[1]=="u"&&$salida[2]=="p"&&$salida[3]=="l"&&$salida[4]=="i"&&$salida[5]=="c"&&$salida[6]=="a"&&$salida[7]=="t"&&$salida[8]=="e"){
			$salida="false";
		}
		$mysqli->close();
		echo json_encode($salida);
	}
	function cerrarSesion(){
		session_start();
		$_SESSION = array();

		// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
		// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// Finalmente, destruir la sesión.
		session_destroy();
	}
	function verificarLogeo(){
		session_start();
		$objeto="";
		if(isset($_SESSION['id'])){
			$objeto[0]="true";
			$objeto[1]=$_SESSION['tipo'];
			echo json_encode($objeto);
		}
		else {
			$objeto[0]="false";
			echo json_encode($objeto);
		}

	}
	function Buscar(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$cedula=$_REQUEST['cedula'];
		$tupla="SELECT * FROM  usuario  WHERE  cedula='$cedula'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[0]['id']=$db_resultado['id'];
			$objeto[0]['nombre']=$db_resultado['nombre'];
			$objeto[0]['cedula']=$db_resultado['cedula'];
			$objeto[0]['cargo']=$db_resultado['cargo'];
			$objeto[0]['tipo']=$db_resultado['tipo'];
			$objeto[0]['unidadgeneral']=$db_resultado['unidadgeneral'];
			$objeto[0]['subunidadgeneral']=$db_resultado['subunidadgeneral'];
			$objeto[0]['unidad']=$db_resultado['unidad'];
			$objeto[0]['unidadgeneral2']=$db_resultado['unidadgeneral2'];
			$objeto[0]['usuario']=$db_resultado['usuario'];
			$objeto[0]['clave']=$db_resultado['clave'];
					
			
		}
		$mysqli->close();
		echo json_encode($objeto);

	}
	function Agregar(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$nombre=$_REQUEST['nombre'];
		$cargo=$_REQUEST['cargo'];
		$tipo=$_REQUEST['tipo'];
		$usuario=$_REQUEST['usuario'];
		$clave=$_REQUEST['clave'];
		$unidadgeneral=$_REQUEST['unidadgeneral'];
		$subunidadgeneral=$_REQUEST['subunidadgeneral'];
		$unidad=$_REQUEST['unidad'];
		$unidadgeneral2=$_REQUEST['unidadgeneral'];
		$cedula=$_REQUEST['cedula'];
		$salida="true";
		$tupla="INSERT INTO usuario (usuario, clave, nombre, cedula, cargo, tipo, unidadgeneral, unidad, subunidadgeneral, unidadgeneral2) 
				VALUES ('$usuario', '$clave', '$nombre', '$cedula', '$cargo', '$tipo', '$unidadgeneral', '$unidad', '$subunidadgeneral', '$unidadgeneral2')";
		$resultado = $mysqli->query($tupla) or  $salida=$mysqli->error;

		if($salida!="true"&&$salida[0]=="D"&&$salida[1]=="u"&&$salida[2]=="p"&&$salida[3]=="l"&&$salida[4]=="i"&&$salida[5]=="c"&&$salida[6]=="a"&&$salida[7]=="t"&&$salida[8]=="e"){
			$salida="false";
		}
		$mysqli->close();
		echo json_encode($salida);		

	}
	function iniciarSesion($email, $pass){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$correo = $email;
		$pass =$pass;
		$tupla = "SELECT * FROM usuario WHERE  usuario='$correo' and clave='$pass'";
		$resultado = $mysqli->query($tupla);
		$salida=false;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			session_start();
			$_SESSION['id'] =$db_resultado['id'];
			$_SESSION['usuario'] =$db_resultado['usuario'];
			$_SESSION['tipo']=$db_resultado['tipo'];
			
			echo "true";
		}
		else {

			echo "false";
		}

		$mysqli->close();
	}

?>