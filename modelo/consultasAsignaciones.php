<?php
	include("conector.php");
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			GenerarInforme();
			break;
		case 2:
			ObtenerInforme();
			break;
		case 3:
			EliminarInforme();
			break;
		case 4:
			modificar();
			break;
		case 5:
			verinformegenerados();
			break;
		case 6:
			ObtenerDetalledeinforme();
			break;
		case 7:
			tiempoderespuesta();
			break;
		case 8:
			tiempoderespuestapromedio();
			break;
		default;

	}

	function tiempoderespuestapromedio(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$fecha1=$_REQUEST['fecha1'];
		$fecha2=$_REQUEST['fecha2'];
		$tupla="SELECT solicitudservicio.fechadeingreso, informe.idsolicitud, informe.fechainforme, solicitudservicio.tipodefalla FROM  solicitudservicio INNER JOIN  informe  on solicitudservicio.id=informe.idsolicitud WHERE  solicitudservicio.fechadeingreso>='$fecha1' AND   informe.fechainforme<='$fecha2' ";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;		
		$i=0;
		$acumulado=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{

			$datetime1 = new DateTime($db_resultado['fechadeingreso']);
			$datetime2 = new DateTime($db_resultado['fechainforme']);
			$interval = $datetime1->diff($datetime2);
			$acumulado=$acumulado+$interval->format('%R%a');
			$objeto[$i]['acumulado']=str_replace("+", "", $interval->format('%d'));
			$i++;

		}
		$objeto[0]['acumulado']=$acumulado;
		$objeto[0]['promedio']=$acumulado/$i;
		$objeto[0]['veces']=$i;
		$mysqli->close();		
		echo json_encode($objeto);

	}
	function tiempoderespuesta(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$fecha1=$_REQUEST['fecha1'];
		$fecha2=$_REQUEST['fecha2'];
		$tupla="SELECT solicitudservicio.fechadeingreso, informe.idsolicitud, informe.fechainforme, solicitudservicio.tipodefalla FROM  solicitudservicio INNER JOIN  informe  on solicitudservicio.id=informe.idsolicitud WHERE  solicitudservicio.fechadeingreso>='$fecha1' AND   informe.fechainforme<='$fecha2' ";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;		
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{

			$datetime1 = new DateTime($db_resultado['fechadeingreso']);
			$datetime2 = new DateTime($db_resultado['fechainforme']);
			$interval = $datetime1->diff($datetime2);
			$objeto[$i]['idsolicitud']=$db_resultado['idsolicitud'];

			$objeto[$i]['fechainforme']=$db_resultado['fechainforme'];
			$date = new DateTime($objeto[$i]['fechainforme']);
			$objeto[$i]['fechainforme']=$date->format('d-m-Y');

			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');
			while(strlen($objeto[$i]['idsolicitud'])<7){
				$objeto[$i]['idsolicitud']="0".$objeto[$i]['idsolicitud'];
			}
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$objeto[$i]['tiempoderespuesta']=str_replace("+", "", $interval->format('%R%a'));
			$i++;

		}
		$mysqli->close();		
		echo json_encode($objeto);

	}

	function ObtenerDetalledeinforme(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$id=$_REQUEST['idInforme'];
		$tupla="SELECT informe.*, usuario.*, solicitudservicio.fechadeingreso, solicitudservicio.hora as hora2, solicitudservicio.estado, solicitudservicio.telefono, solicitudservicio.tipodefalla, solicitudservicio.descripciondefalla FROM  informe INNER JOIN solicitudservicio on solicitudservicio.id=informe.idsolicitud INNER JOIN  usuario  on usuario.id=solicitudservicio.idFuncionario WHERE  informe.id='$id'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;		
		$i=0;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['diagnostico']=$db_resultado['diagnostico'];
			$objeto[$i]['solucion']=$db_resultado['solucion'];
			$objeto[$i]['soporte']=$db_resultado['soporte'];
			$objeto[$i]['idsolicitud']=$db_resultado['idsolicitud'];
			while(strlen($objeto[$i]['idsolicitud'])<7){
				$objeto[$i]['idsolicitud']="0".$objeto[$i]['idsolicitud'];
			}
			$objeto[$i]['observacion']=$db_resultado['observacion'];

			$objeto[$i]['id']=$db_resultado['id'];
			$objeto[$i]['hora1']=$db_resultado['hora'];
			$objeto[$i]['hora2']=$db_resultado['hora2'];
			$objeto[$i]['fechainforme']=$db_resultado['fechainforme'];
			$date = new DateTime($objeto[$i]['fechainforme']);
			$objeto[$i]['fechainforme']=$date->format('d-m-Y');

			$objeto[$i]['fechadeingreso']=$db_resultado['fechadeingreso'];
			$date = new DateTime($objeto[$i]['fechadeingreso']);
			$objeto[$i]['fechadeingreso']=$date->format('d-m-Y');


			$objeto[$i]['unidadgeneral']=$db_resultado['unidadgeneral'];
			$objeto[$i]['subunidadgeneral']=$db_resultado['subunidadgeneral'];
			$objeto[$i]['unidad']=$db_resultado['unidad'];
			$objeto[$i]['unidadgeneral2']=$db_resultado['unidadgeneral2'];
			$objeto[$i]['estado']=$db_resultado['estado'];
			$objeto[$i]['telefono']=$db_resultado['telefono'];
			$objeto[$i]['tipodefalla']=$db_resultado['tipodefalla'];
			$objeto[$i]['descripciondefalla']=$db_resultado['descripciondefalla'];

		}
		$mysqli->close();		
		echo json_encode($objeto);
	}
	
	function verinformegenerados(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$tupla="SELECT * FROM  informe";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;		
		$i=0;
		while($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[$i]['diagnostico']=$db_resultado['diagnostico'];
			$objeto[$i]['solucion']=$db_resultado['solucion'];
			$objeto[$i]['soporte']=$db_resultado['soporte'];
			$objeto[$i]['idsolicitud']=$db_resultado['idsolicitud'];
			while(strlen($objeto[$i]['idsolicitud'])<7){
				$objeto[$i]['idsolicitud']="0".$objeto[$i]['idsolicitud'];
			}
			$objeto[$i]['id']=$db_resultado['id'];
			$objeto[$i]['hora']=$db_resultado['hora'];
			$objeto[$i]['fechainforme']=$db_resultado['fechainforme'];
			$date = new DateTime($objeto[$i]['fechainforme']);
			$objeto[$i]['fechainforme']=$date->format('d-m-Y');			
			$i++;
		}
		$mysqli->close();		
		echo json_encode($objeto);

	}
	function modificar(){
		session_start();		
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$diagnostico=$_REQUEST['diagnostico'];
		$solucion=$_REQUEST['solucion'];
		$observacion=$_REQUEST['observacion'];
		$estado=$_REQUEST['estado'];
		$soporte=$_REQUEST['soporte'];
		$numero=$_REQUEST['numero'];
		$idtecnico=$_SESSION['id'];
		$salidad="true";
		$tupla="UPDATE informe SET diagnostico='$diagnostico', solucion='$solucion', observacion='$observacion', soporte='$soporte' WHERE  idsolicitud='$numero'";
		$resultado = $mysqli->query($tupla) or $salidad=$mysqli->error;

		$tupla="UPDATE solicitudservicio SET  estado='$estado' WHERE  id='$numero'";
		$resultado = $mysqli->query($tupla);		
		$mysqli->close();		
		echo json_encode($salidad);
	} 
	function EliminarInforme(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$numero=$_REQUEST['numero'];
		$tupla="DELETE FROM informe WHERE  idsolicitud='$numero'";
		$resultado = $mysqli->query($tupla);
		$mysqli->close();		
		echo json_encode("true");
	}
	function ObtenerInforme(){
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$numero=$_REQUEST['numero'];
		$tupla="SELECT  *  FROM informe INNER JOIN  solicitudservicio on informe.idsolicitud=solicitudservicio.id WHERE  idsolicitud='$numero'";
		$resultado = $mysqli->query($tupla);
		$objeto[0]['m']=$resultado->num_rows;
		
		$i=0;
		if($db_resultado = mysqli_fetch_array($resultado, MYSQLI_ASSOC))
		{
			$objeto[0]['diagnostico']=$db_resultado['diagnostico'];
			$objeto[0]['solucion']=$db_resultado['solucion'];
			$objeto[0]['soporte']=$db_resultado['soporte'];
			$objeto[0]['observacion']=$db_resultado['observacion'];

			$objeto[0]['estado']=$db_resultado['estado'];
			$objeto[0]['numero']=$numero;


		}
		$mysqli->close();		
		echo json_encode($objeto);
	}
	function GenerarInforme(){
		session_start();
		$mysqli = new mysqli(Host, User, Pass, BasedeDatos);
		$diagnostico=$_REQUEST['diagnostico'];
		$solucion=$_REQUEST['solucion'];
		$observacion=$_REQUEST['observacion'];
		$estado=$_REQUEST['estado'];
		$soporte=$_REQUEST['soporte'];
		$numero=$_REQUEST['numero'];
		$idtecnico=$_SESSION['id'];
		$date=date('Y-m-d');
		$hora=$_REQUEST['reloj'];
		$tupla="INSERT INTO  informe (idTecnico, idsolicitud, diagnostico, solucion, observacion, soporte, fechainforme, hora) VALUES ('$idtecnico', '$numero', '$diagnostico', '$solucion', '$observacion', '$soporte','$date', '$hora')";
		$resultado = $mysqli->query($tupla);

		$tupla="UPDATE solicitudservicio SET  estado='$estado' WHERE  id='$numero'";
		$resultado = $mysqli->query($tupla);		
		$mysqli->close();		
		echo json_encode("true");
	}
?>